package ph.stacktrek.novare.snakeandladder.johnlloyd.paranal.snakeandladder

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.google.firebase.crashlytics.buildtools.reloc.com.google.common.reflect.TypeToken
import com.google.gson.Gson
import ph.stacktrek.novare.snakeandladder.johnlloyd.paranal.snakeandladder.databinding.ActivityGameBinding
class GameBoard {
    val boardSize = 100
    private val snakes = mapOf(
        17 to 7,
        54 to 34,
        62 to 19,
        64 to 19,
        87 to 24,
        93 to 73,
        95 to 75,
        98 to 79
    )
    private val ladders = mapOf(
        1 to 38,
        4 to 14,
        9 to 31,
        21 to 42,
        28 to 84,
        51 to 67,
        71 to 91,
        80 to 100
    )
    fun getNextPosition(currentPosition: Int, diceRoll: Int): Int {
        var newPosition = currentPosition + diceRoll

        if (newPosition > boardSize) {
            newPosition = boardSize - (newPosition - boardSize)
        }

        if (snakes.containsKey(newPosition)) {
            newPosition = snakes.getValue(newPosition)
        }

        if (ladders.containsKey(newPosition)) {
            newPosition = ladders.getValue(newPosition)
        }

        return newPosition
    }
}
class Player(val name: String) {
    var position = 0
    private val boardWidth = 1000 // width of board_image ImageView in pixels
    private val boardHeight = 805 // height of board_image ImageView in pixels
    private val numCols = 10 // number of columns on the board
    private val numRows = 10 // number of rows on the board

    fun getPlayerPositionX(): Float {
        val row = (position - 1) / numCols
        val col = if (row % 2 == 0) {
            (position - 1) % numCols
        } else {
            numCols - 1 - (position - 1) % numCols
        }
        val tileWidth = boardWidth / numCols.toFloat()
        return col * tileWidth + tileWidth / 2
    }

    fun getPlayerPositionY(): Float {
        val row = (position - 1) / numCols
        val adjustedRow = numRows - 1 - row
        val tileHeight = boardHeight / numRows.toFloat()
        return adjustedRow * tileHeight + tileHeight / 2
    }
}


class GameActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_NUM_PLAYERS = "extra_num_players"
        const val EXTRA_PLAYER_NAMES = "extra_player_names"
    }

    private lateinit var binding: ActivityGameBinding
    private lateinit var playerMarkers: List<ImageView>
    private lateinit var gameBoard: GameBoard
    private lateinit var diceImage: ImageView
    private lateinit var scoreText: TextView
    private lateinit var currentPlayerText: TextView
    private lateinit var players: List<Player>
    private var currentPlayerIndex = 0

    private var winner: String? = null // add a variable to store the winner's name

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGameBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Get the number of players from the intent extras
        val numPlayers = intent.getIntExtra(EXTRA_NUM_PLAYERS, 1)

        val playerNames = intent.getStringArrayListExtra(EXTRA_PLAYER_NAMES)
        if (playerNames != null && playerNames.size == numPlayers) {
            // Use the player names from the intent if they were provided
            players = playerNames.map { Player(it) }
        }

        // Initialize the game board and players
        gameBoard = GameBoard()
        playerMarkers = listOf(
            binding.playerMarker1,
            binding.playerMarker2,
            binding.playerMarker3,
            binding.playerMarker4
        )

        // Get UI components
        diceImage = binding.diceImage
        scoreText = binding.scoreText
        currentPlayerText = binding.currentPlayerText

        // Set initial UI text
        updateUI()

        // Set onClickListener for roll button
        binding.rollButton.setOnClickListener { rollDice() }

    }

    private fun rollDice() {
        // Generate a random number between 1 and 6
        val diceRoll = (1..6).random()

        // Update the dice image
        val drawableResource = when (diceRoll) {
            1 -> R.drawable.dice1
            2 -> R.drawable.dice2
            3 -> R.drawable.dice3
            4 -> R.drawable.dice4
            5 -> R.drawable.dice5
            else -> R.drawable.dice6
        }
        diceImage.setImageResource(drawableResource)

        // Update the current player's position on the game board
        val currentPlayer = players[currentPlayerIndex]
        currentPlayer.position = gameBoard.getNextPosition(currentPlayer.position, diceRoll)

        // Check if the current player has won
        if (currentPlayer.position == gameBoard.boardSize) {
            val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_winner, null)

            val dialogBuilder = AlertDialog.Builder(this)
                .setView(dialogView)
                .setCancelable(false)

            val winnerTextView = dialogView.findViewById<TextView>(R.id.winner_text)
            winnerTextView.text = "${currentPlayer.name} wins!"
            saveWinner(currentPlayer.name)
            val dialog = dialogBuilder.create()
            dialog.show()

            val okButton = dialogView.findViewById<Button>(R.id.ok_button)
            okButton.setOnClickListener {
                dialog.dismiss()
                // Redirect to home page
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }

        // Update the current player and UI
        currentPlayerIndex = (currentPlayerIndex + 1) % players.size
        updateUI()
    }
    private fun saveWinner(name: String) {
        val sharedPreferences = getSharedPreferences("winners", Context.MODE_PRIVATE)
        val winnersJson = sharedPreferences.getString("winners", "[]") ?: "[]"
        val winnersList = Gson().fromJson(winnersJson, object : TypeToken<List<String>>() {}.type) as MutableList<String>

        winnersList.add(name)
        sharedPreferences.edit().putString("winners", Gson().toJson(winnersList)).apply()
    }

    private fun updateUI() {
        // Update the score and current player text
        val currentPlayer = players[currentPlayerIndex]
        scoreText.text = "Score: ${currentPlayer.position}"
        currentPlayerText.text = "Current player: ${currentPlayer.name}"

        // Update the position of all player markers
        for ((index, player) in players.withIndex()) {
            playerMarkers[index].translationX = player.getPlayerPositionX()
            playerMarkers[index].translationY = player.getPlayerPositionY()

        }
    }
}