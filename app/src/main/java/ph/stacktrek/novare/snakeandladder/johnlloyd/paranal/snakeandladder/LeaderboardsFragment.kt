package ph.stacktrek.novare.snakeandladder.johnlloyd.paranal.snakeandladder

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.crashlytics.buildtools.reloc.com.google.common.reflect.TypeToken
import com.google.gson.Gson
import ph.stacktrek.novare.snakeandladder.johnlloyd.paranal.snakeandladder.databinding.FragmentLeaderboardsBinding

class LeaderboardsFragment : Fragment() {

    private var _binding: FragmentLeaderboardsBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLeaderboardsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val winners = fetchWinners()
        val adapter = WinnersAdapter(winners)
        binding.winnersRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.winnersRecyclerView.adapter = adapter
    }

//     private fun saveWinner(name: String) {
//        val sharedPreferences = requireActivity().getSharedPreferences("winners", Context.MODE_PRIVATE)
//        val winnersJson = sharedPreferences.getString("winners", "[]") ?: "[]"
//        val winnersList = Gson().fromJson(winnersJson, object : TypeToken<List<String>>() {}.type) as MutableList<String>
//
//        winnersList.add(name)
//        sharedPreferences.edit().putString("winners", Gson().toJson(winnersList)).apply()
//    }

    private fun fetchWinners(): List<String> {
        val sharedPreferences = requireActivity().getSharedPreferences("winners", Context.MODE_PRIVATE)
        val winnersJson = sharedPreferences.getString("winners", "[]") ?: "[]"
        val winnersList = Gson().fromJson(winnersJson, object : TypeToken<List<String>>() {}.type) as MutableList<String>

        // Return the latest 5 winners
        return if (winnersList.size > 5) {
            winnersList.takeLast(5)
        } else {
            winnersList
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}